<?php // $Id$

/**
 * @file
 * System functions for the watchlist module.
 */

/**
 * Callback for implementation of hook_menu().
 */
function _watch_menu() {

  // An event that causes a menu_rebuild may have added base tables to Views.
  watch_system_check_base_tables();

  // Views and/or displays may have been removed.  Clear them from watch.
  watch_system_cleanup();

  $path = drupal_get_path('module', 'watch') .'/includes';
  $items = array();
 // $items['watch/%/%view'] = array(
  $items['watch'] = array(
    'title' => 'Watch',
    'page callback' => 'watch_user_page',
    //'page arguments' => array(1, 2),
    'access callback' => 'user_access',
    'access arguments' => array('create watchlists'),
    'type' => MENU_CALLBACK,
    'file' => 'watch.user.inc',
    'file path' => $path,
  );
  return $items;
}

function watch_system_check_base_tables() {
  // An event that causes a menu_rebuild may have added base_tables to Views.
  $base_tables = variable_get('watch_base_tables', array());

  // Account for all base tables in the watch_queue table.
  if (function_exists('views_fetch_data')) {
    foreach (views_fetch_data() as $table => $info) {
      if (!empty($info['table']['base']) && !isset($base_tables[$table])) {
        $field = $info['table']['base']['field'];
        $base_tables[$table] = array(
          'field' => $field,
          'table' => 'watch_queue_'. $table,
        );
      }
    }
  }

  // Create new tables, if needed.
  if ($base_tables != variable_get('watch_base_tables', array())) {
    variable_set('watch_base_tables', $base_tables);

    $schema = drupal_get_schema(NULL, TRUE);
    $ret = array();
    foreach ($base_tables as $table => $info) {
      if (!db_table_exists($info['table'])) {
        db_create_table($ret, $info['table'], $schema[$info['table']]);
      }
    }
  }
}

function watch_system_cleanup() {
  // Find all watched views/displays.
  $res = db_query("SELECT DISTINCT wid, view, display FROM {watch}
    ORDER BY view, display");

  // Find all watched views/displays.
  $view_last = $view_disabled = $FALSE;
  while ($row = db_fetch_object($res)) {
    if ($view_last != $row->view) {
      $view_last = $row->view;
      $view_disabled = FALSE;

      if (!$view = views_get_view($row->view)) {
        db_query("UPDATE {watch} SET status = 0 WHERE view = '%s'", $row->view);
        $view_disabled = TRUE;
      }
    }

    if ($view_disabled) continue;

    if (!isset($view->display[$row->display])) {
      db_query("UPDATE {watch} SET status = 0
        WHERE view = '%s' AND display = '%s'", $row->view, $row->display);
    }
  }

  // Disable orphaned watchlists.
  db_query("UPDATE {watch} w LEFT JOIN {watch_subscription} ws USING ( wid )
    SET w.status = 0 WHERE (w.global = 0) AND (ws.wid IS NULL)");

  // Make sure all the queues for disabled watchlists are empty.
  $res = db_query("SELECT * from {watch} WHERE status = 0");
  while ($row = db_fetch_object($res)) {
    $watch = watch_load($row);
    $base = watch_queue_table($watch->base_table);
    db_query("DELETE FROM {". $base['table'] ."} WHERE wid = %d", $watch->wid);
  }
}
