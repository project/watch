<?php // $Id$

/**
 * @file
 * Activities to queue, run and notify watchlist subscribers of new information.
 */

/**
 * Run selected watchlist queries when affected objects are updated.  Any
 * watchlist subscription with a frequency of 0 is subject to examination.
 */
function watch_run_immediate($base_table, $object) {
  $base_tables = variable_get('watch_base_tables', array());
  if (!$base = $base_tables[$base_table]) return FALSE;

  // Find all subscriptions that run as immediate for this base view type.
  $res = db_query("SELECT DISTINCT w.* FROM {watch} w
    INNER JOIN {watch_subscription} ws USING ( wid )
    WHERE ws.frequency = 0 AND w.base_table = '%s'", $base_table);

  // Refresh the watch queues for all affected watchlists.
  $wids = array();
  while ($watch = watch_load(db_fetch_object($res))) {
    if (watch_run_queue($watch)) {
      $wids[] = $watch->wid;
    }
  }

  // No watchlists were updated.  Nothing more to do here.
  if (!$wids) return;

  // See if our object appears anywhere in the watch queue table.
  $res = db_query("SELECT DISTINCT w.* FROM {watch} w
    LEFT JOIN {%s} wq USING ( wid )
    WHERE wq.%s = %d AND w.wid IN (%s)"
    , $base['table'], $base['field'], $object->$base['field'], join(', ', $wids));

  // Notify all affected subscribers.
  while ($watch = watch_load(db_fetch_object($res))) {
    watch_run_notify($watch, $object);
  }
}

/**
 * Run a scheduled search for new watchlist content.
 *
 * All watchlist views are executed, but results are added to a watchlist_queue
 * table.  This permits us to efficiently generate feeds of watchlist items
 * and defer the notification process.
 */
function watch_run_queue($list = array()) {
  global $user;
  $ret = FALSE;

  // Queue the entries using user 1.  This means we can store the same queue
  // for everyone, and re-query it on view/notify to respect node_access.
  $real_user = $user;
  $user = user_load(1);

  if (!is_array($list)) $list = array($list);

  // No watchlists specified.  Run them all to populate all queue entries.
  if (!$list) {
    $res = db_query("SELECT * from {watch} WHERE status = 1");
    while ($row = db_fetch_object($res)) {
      $list[] = watch_load($row);
    }
  }

  foreach ($list as $watch) {
    $base = variable_get('watch_base_tables', array());
    $base = $base[$watch->base_table];

    // Get the view.
    $view = views_get_view($watch->view);
    $view->set_display($watch->display);
    //$view->set_arguments($watch->args);  // TODO
    //$view->set_exposed_input($watch->filters);  // TODO

    // Limit returned fields to the wid and a unique id ( nid, uid, vid, etc. )
    $time = time();
    $view->set_use_pager(FALSE);
    $view->build();

    // Exclude any fields not pertinent to the watch_queue table.
    $view->query->orderby = array();
    $view->query->clear_fields();
    $view->query->add_field(null, $watch->wid, 'watch_wid');
    $view->query->add_field($watch->base_table, $base['field']);
    $view->query->add_field(null, time(), 'watch_timestamp');

    // Add a join for this watchlist to ensure the items aren't already there.
    $join = views_get_table_join($base['table'], $watch->base_table);
    $join->extra[] = array('field' => 'wid', 'value' => $watch->wid, 'numeric' => TRUE);

    $watch_table = $view->query->add_table($base['table'], NULL, $join);
    $view->query->add_where(0, $watch_table .'.'. $base['field'] .' IS NULL');

    // Prepend "INSERT INTO" to place the entire result set in the queue table.
    $view->build_info['query'] = "INSERT INTO {". $base['table'] ."} (wid, ". $base['field'] .", timestamp) " . $view->query->query();

    // Run the query, which will deposit all results into a watch_queue_* table.
    // Swallowing errors because a db_fetch_object has nothing to grab from our
    // INSERT statement and results in an error.
    @$view->execute();

    // Keep track of whether or not we're making any changes.
    $ret = $ret || db_affected_rows();
  }

  // Put the user object back the way we found it.
  $user = $real_user;

  return $ret;
}

/**
 * Execute each watchlist that has new content, and deliver it using any
 * module or modules that implements hook_watch_notify().
 *
 * By this point, the pertinent items should already exist in the queue table.
 * Re-executing the view with watch_run_query() will guarantee that node access
 * settings and/or tokens are respected.
 */
function watch_run_notify($watch = NULL, $object = NULL) {
  $count = 0;

  // If no module implements hook_watch_notify, there's no point in proceeding.
  if (!$modules = module_implements('watch_notify')) {
    return $count;
  }

  // Filter the subscriptions by watchlist, if provided.
  $filter = $watch ? 'AND wid ='. (int) $watch->wid : '';

  if ($object) {
    // Deliver only "instant" updates.
    $res = db_query("SELECT * FROM {watch_subscription} ws
      LEFT JOIN {watch} w USING ( wid )
      WHERE w.status = 0 AND ws.frequency = 0 $filter
      ORDER BY w.wid");
  }
  else {
    // Deliver batches of updates that are due.
    $res = db_query("SELECT * FROM {watch_subscription} ws
      LEFT JOIN {watch} w USING ( wid )
      WHERE w.status = 1
      AND ws.frequency IS NOT NULL
      AND ( ws.frequency + ws.lastrun ) <= %d $filter
      ORDERY BY w.wid", time());
  }

  $last_wid = 0;
  while ($row = db_fetch_object($res)) {
    if ($row->wid =! $last_wid) {
      $watch = watch_load($row->wid);
      $last_wid = $watch->wid;
    }

    $account = user_load($row->uid);
    if (!$account->uid) $account->mail = $row->mail;

    $result = array();
    $output = watch_run_query($watch, $result, $account, $object, $row->lastrun);
    // Update the lastrun timestamp.
    $edit = array('lastrun' => time(), 'wsid' => $row->wsid);
    drupal_write_record('watch_subscription', $edit, 'wsid');

    // If we were looking for a specific object, don't return bogus output.
    if ($object && !$result) {
      continue;
    }

    // Call the notify hooks.
    foreach ($modules as $module) {
      $func = $module .'_watch_notify';
      if (is_int($ret = $func($watch, $account, $row, $output, $result))) {
        $count += $ret;
      }
    }
  }

  // Announce that we're done sending notifications for now.
  module_invoke_all('watch_notify_done');

  watchdog('watch', 'Sent %count watchlist results', array('%count' => $count));
  return $count;
}

/**
 * Called from watch_run_notify, execute a view for new watchlist content.
 *
 * By this point, the pertinent items exist in the queue table,
 * The the view is re-executed to ensure that any user-specific permissions
 * and settings are respected.
 */
function watch_run_query($watch, &$result, $account = NULL, $object = NULL, $lastrun = NULL) {
  global $user;
  $real_user = $user;

  if ($account) {
    $user = $account;
  }

  // Get the view.
  $view = views_get_view($watch->view);
  $view->set_display($watch->display);

  // Set some values which will be picked up by watch_views_query_alter().
  $view->_watch = $watch;
  $view->_watch_object = $object;
  $view->_watch_lastrun = $lastrun;

  // Make it so!
  $output = $view->render();
  $result = $view->result;

  // Leave the user object how we found it.
  $user = $real_user;

  return $output;
}

/**
 * Implementation of hook_views_query_alter().
 * It's in this file because it won't need to be called on normal views.
 * Affects views called by watch_run_query() for per-user results.
 */
function watch_views_query_alter(&$view, &$query) {
  // Only looking for a specific object (e.g., the one that's being updated).
  if (isset($view->_watch_object)) {
    $base_tables = variable_get('watch_base_tables', array());
    $base = $base_tables[$view->_watch->base_table];
    $table = $view->query->ensure_table($view->_watch->base_table);
    $where = $table .'.'. $base['field'] .' = %d ';

    $view->query->add_where(0, $where, $view->_watch_object->{$base['field']});
  }

  // Filter by last execution time to find new results.
  elseif (isset($view->_watch_lastrun)) {
    // Add a join to the queue table.
    $join = views_get_table_join($base['table'], $view->_watch->base_table);
    $join->extra[] = array(
      'field' => 'wid',
      'value' => $view->_watch->wid,
      'numeric' => TRUE
    );
    $watch_table = $view->query->add_table($base['table'], NULL, $join);

    // Filter on the timestamp, trying to avoid duplicates on the next run.
    $view->query->add_where(0, $watch_table .'.timestamp > %d ', $view->_watch_lastrun);
  }
}
