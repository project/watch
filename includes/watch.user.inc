<?php // $Id$
/**
 * @file
 *
 * User subscriptions and preferences for watchlists.
 */

function watch_user_page($op, $view = NULL, $display = NULL, $args = array(), $filter = NULL) {
  if ($args) $args = unserialize(base64_decode($args));
  if ($filter) $filter = unserialize(base64_decode($filter));
  $watch = watch_user_init($view, $display, $args, $filter);
  $result = watch_user_subscription($watch, $op);
return ' ';
}

function watch_user_init($view, $display = 'default', $args = array(), $filter = array()) {
  if (!is_object($view)) {
    $view = views_get_view($view);
  }

  // Generate a unique hash for this collection of parameters.
  $hash = md5($view->name . $display . join('/', $args) . serialize($filter));

  $res = db_query("SELECT * FROM {watch} WHERE hash = '%s'", $hash);
  if ($watch = db_fetch_object($res)) {
    return watch_load($watch);
  }
  $watch = array(
    'base_table' => $view->base_table,
    'view' => $view->name,
    'status' => 1,
    'display' => $display,
    'args' => $args ? join('/', $args) : NULL,
    'filters' => $filters ? $filters : NULL,
    'global' => FALSE,
    'hash' => $hash,
  );

  drupal_write_record('watch', $watch);
  return watch_load((object)$watch);
}

function watch_user_subscription($watch, $op, $account = NULL) {
  if (!$account) {
    global $user;
    $account = $user;
  }

  switch ($op) {

    case 'add':
      if (db_result(db_query("SELECT wsid FROM {watch_subscription}
        WHERE wid = %d
        AND ((uid = 0 AND mail = '%s') OR ( uid = '%d'))", $watch->wid, $account->mail, $account->uid))) {
        return TRUE;
      }
      $subscription = array(
        'wid' => $watch->wid,
        'uid' => $account->uid,
        'created' => time(),
        'lastrun' => time(),
      );
      return drupal_write_record('watch_subscription', $subscription);

    case 'delete':
      // Delete this subscription.
      db_query("DELETE FROM {watch_subscription}
        WHERE wid = %d
        AND ((uid = 0 AND mail = '%s') OR ( uid = '%d'))", $watch->wid, $account->mail, $account->uid);

      return TRUE;
  }
}
