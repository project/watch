<?php // $Id$

/**
 * @file
 * Views API for the Watch module.
 */

/**
 * Implementation of hook_views_data().
 */
function watch_views_data() {
  $data = array();
  foreach (variable_get('watch_base_tables', array()) as $name => $base) {
    $data[$base['table']]['table']['join'] = array(
      $name => array(
        'field' => $base['field'],
        'left_field' => $base['field'],
      ),
    );

    $data[$base['table']]['wid'] = array(
      'title' => t('Watchlist'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    );
  }
  return $data;
}

/**
 * Implementation of hook_views_plugins().
 */
function watch_views_plugins() {
  return array(
    'module' => 'watch',
    'display' => array(
      'watch' => array(
        'title' => t('Watchlist'),
        'help' => t('Allow users to subscribe to updated content for this view.  The Fields and Basic Settings affect how results appear when delivered to subscribers.'),
        'handler' => 'views_plugin_display_watch',
        'path' => drupal_get_path('module', 'watch') .'/includes/views',
        'uses hook menu' => FALSE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'admin' => t('Watch'),
      ),
    ),
  );
}
