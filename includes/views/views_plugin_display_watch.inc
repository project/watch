<?php // $Id$

/**
 * @file
 * Watchlist display plugin for Views.
 *
 */

class views_plugin_display_watch extends views_plugin_display {

  function option_definition() {
    $options = parent::option_definition();

    $options['format'] = array('default' => 'link');
    $options['position'] = array('default' => 'before');
    $options['displays'] = array('default' => array());

    return $options;
  }

  /**
   * Provide the summary for attachment options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);

    $categories['watch'] = array(
      'title' => t('Watchlist settings'),
    );

    $displays = array_filter($this->get_option('displays'));
    if (count($displays) > 1) {
      $attach_to = t('Multiple displays');
    }
    else if (count($displays) == 1) {
      $display = array_shift($displays);
      if (!empty($this->view->display[$display])) {
        $attach_to = $this->view->display[$display]->display_title;
      }
    }

    if (!isset($attach_to)) {
      $attach_to = t('None');
    }

    $options['displays'] = array(
      'category' => 'watch',
      'title' => t('Attach to'),
      'value' => $attach_to,
    );

    $options['format'] = array(
      'category' => 'watch',
      'title' => t('Subscription interface'),
      'value' => $this->get_option('format'),
    );

    $options['position'] = array(
      'category' => 'watch',
      'title' => t('Subscription position'),
      'value' => $this->get_option('position'),
    );
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'format':
        $form['#title'] .= t('Subscription format');
        $form['format'] = array(
          '#type' => 'radios',
          '#description' => t('Select the format of the subscription interface on this display.'),
          '#options' => array('link' => t('Link'), 'form' => t('Form')),
          '#default_value' => $this->get_option('format'),
        );
        break;

      case 'position':
        $form['#title'] .= t('Subscription position');
        $form['format'] = array(
          '#type' => 'radios',
          '#description' => t('Select the placement of the subscription interface on this display.'),
          //'#options' => array('before' => t('Before'), 'after' => t('After'), 'block' => t('Block')),
          '#options' => array('before'),
          '#default_value' => $this->get_option('position'),
        );
        break;

      case 'displays':
        $form['#title'] .= t('Attach to');
        $displays = array();
        foreach ($this->view->display as $display_id => $display) {
          if (!empty($display->handler) && $display->handler->accept_attachments()) {
            $displays[$display_id] = $display->display_title;
          }
        }
        $form['displays'] = array(
          '#type' => 'checkboxes',
          '#description' => t('Select the display or displays where the visitor will be presented with the watchlist form.'),
          '#options' => $displays,
          '#default_value' => $this->get_option('displays'),
        );
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);

    switch ($form_state['section']) {
      case 'position':
      case 'format':
      case 'displays':
        $this->set_option($form_state['section'], $form_state['values'][$form_state['section']]);
        break;
    }
  }

  /**
   * Attach to another display.
   */
  function attach_to($display_id) {

    // Make sure the users can access this display.
    if (!user_access('create watchlists')) return;

    $displays = $this->get_option('displays');
    if (empty($displays[$display_id])) {
      return;
    }

    $attachment = $this->watch_interface();

    // TODO position is coming through as "broken field" :(
    $this->view->attachment_before .= $attachment;

    switch ($this->get_option('position')) {
      case 'before':
        $this->view->attachment_before .= $attachment;
        break;

      case 'after':
        $this->view->attachment_after .= $attachment;
        break;

      case 'both':
        $this->view->attachment_before .= $attachment;
        $this->view->attachment_after .= $attachment;
        break;
    }
  }

  function watch_interface() {
    switch ($this->get_option('format')) {
      case 'link':
        $link = 'watch/add';
        $link .= '/'. $this->view->name;
        $link .= '/'. $this->view->current_display;
        $link .= '/'. base64_encode(serialize($this->view->args));
        $link .= '/'. base64_encode(serialize($this->view->exposed_input));
        return l('Watch!', $link);
        break;

      case 'form':
        break;
    }
  }
}
